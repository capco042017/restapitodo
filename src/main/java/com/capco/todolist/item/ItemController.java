package com.capco.todolist.item;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	@RequestMapping("/list")
	public List<Item> getAllItems() {
		return itemService.getAllItems();
	}

	@RequestMapping("/{id}")
	public Item getItem(@PathVariable Long id ) {
		return itemService.getItem(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/add")
	public void addToDoTask(@RequestBody Item item) {
		itemService.addItem(item);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/update")
	public void updateToDoTask(@RequestBody Item item) {
		itemService.updateItem(item);
	}

	@RequestMapping(method=RequestMethod.DELETE, value="/delete")
	public void deleteToDoTask(@RequestBody Long id ) {
		 itemService.deleteItem(id);
	}

	@RequestMapping(method=RequestMethod.PUT, value="/markImportant")
	public void updateMarkImportant(@RequestBody Item item) {
		Item it = itemService.getItem(item.getId());
		it.setImportant(item.isImportant());
		itemService.updateItem(it);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/markComplete")
	public void updateMarkComplete(@RequestBody Item item) {
		Item it = itemService.getItem(item.getId());
		it.setComplete(item.isComplete());
		itemService.updateItem(it);
	}
}

