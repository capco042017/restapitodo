package com.capco.todolist.item;


import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="item")
public class Item {
	
		
	@Id
  	@GeneratedValue(strategy=GenerationType.AUTO)
  	@Column(name="id")
	private long id;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="date", nullable=false)
	private Date date;
	
	private boolean complete;
	private boolean important;
	
	public Item() {
		
	}
		
	public Item(String name, Date date) {
		super();
		this.name = name;
		this.date = date;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
			this.date = date;
	}
	
	
	public boolean isImportant() {
		return important;
	}
	public void setImportant(boolean important) {
		this.important = important;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	

}
